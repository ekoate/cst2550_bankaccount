import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccount 
{
    private double balance;
    private Lock balanceLock;
    private Condition sufficientFunds;

    public BankAccount() 
    {
        balance = 0;
        balanceLock = new ReentrantLock();
        sufficientFunds = balanceLock.newCondition();
    }

    public void deposit(double amount) 
    {
        balanceLock.lock();
        try{
        System.out.print("Depositing " + amount);
        double newBalance = balance + amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;
        sufficientFunds.signalAll();
    }finally{
        balanceLock.unlock();
    }
    }

    public void withdraw(double amount) 
    {
        try {

            balanceLock.lock();
            while (amount > balance)
                sufficientFunds.await();
                
            System.out.print("Withdrawing " + amount);
            double newBalance = balance - amount;
            System.out.println(", new balance is " + newBalance);
            balance = newBalance;
            

        } catch (InterruptedException ex) {

        }finally{
            balanceLock.unlock();
        }
    }

    public double getBalance() 
    {
        return balance;
    }
}
